/*
=============================================================================================
Data Management
=============================================================================================
*/

function writeInFile(data,x){

	var user = JSON.parse(localStorage[data]);
	x.forEach(element => {
		user[element[0]]=element[1];
		localStorage[data] = JSON.stringify(user);
	});
}


/*
=============================================================================================
Dreamer Mode: Drawing management
=============================================================================================
*/

// Display the request of the dreamer, including its status and the drawing if the status is "done"
function displayDrawings(){ 
	var dreamerUserName = sessionStorage.getItem("current_user"); 
	var reqID = null; 
	var toDisplayNew = []; 
	var toDisplayInProg = []; 
	var toDisplayDone = []; 
	Object.keys(localStorage).forEach(function(key){ 
		if(!isNaN(key)){ 
			var data = JSON.parse(localStorage.getItem(key)); 
			if (data["dreamerUserName"]===dreamerUserName){ 
				reqID = data["id"]; 
				if (data["state"]=="New") { 
					toDisplayNew.push(localStorage.getItem(reqID)); } 
					else if (data["state"]=="In progress") { 
						toDisplayInProg.push(localStorage.getItem(reqID)); } 
						else if (data["state"]=="Done") { 
							toDisplayDone.push(localStorage.getItem(reqID)); } 
							document.getElementById("new").innerHTML=` 
							${toDisplayNew.map(notDoneTemplate).join('')} ` 
							document.getElementById("inprogress").innerHTML=` 
							${toDisplayInProg.map(notDoneTemplate).join('')} ` 
							document.getElementById("done").innerHTML=` 
							${toDisplayDone.map(doneTemplate).join('')} ` } 
						}
					});
				}

//Template of the Requests that haven't been drawn
function notDoneTemplate(listOfRequest) { 
	var request = JSON.parse(listOfRequest); 
	console.log(request["id"]); 
	return ` 
	<div class="request"> 
	<p class="requestID">${request["id"]}</p> 
	<p class="requestDreamerUsername">${request["dreamerUserName"]}</p> 
	<p class="requestCategory">${request["category"]}</p> 
	<p class="requestInput">${request["input"]}</p> </div> `; } 


//Post a comment on the drawing to give a feedback to the drawer [To Implement For v2]
function postComment(){
	var comment = document.getElementById("postfeedback").value;
}

//Upload a recording of the dream [To Implement For v2]
function uploadRecording(){
}

//Upload the Request of the dreamer
function uploadRequest(){

	var id = JSON.parse(localStorage["id"]);
	console.log(id);
	id = parseInt(id) + 1;
	console.log(id);
	
	localStorage.setItem("id",id);
	var input = document.getElementById("tex").value;
	var category = document.getElementById("category").value;
	var dreamerUserName = sessionStorage.getItem("current_user");
    var state = "New";
	var url = null;
	localStorage.setItem(id, `{ "id":"100",
	"input": "null",
	"category": "null",
	"dreamerUserName": "null",
	"drawerUserName": "null",
	"state": "null",
	"url": "null"
	}`)
	
	var x = [["id",id],["input",input],["category", category],["dreamerUserName",dreamerUserName],["state",state]];
	console.log(id.toString())
	writeInFile(id,x);
	window.location.href = "dream-sent.html";

}


/*
=============================================================================================
Drawer Mode: Requests management
=============================================================================================
*/

//Display the requests to the drawer so that he/she can accept it.
function displayRequestForDrawer(){
	
	var drawerUserName = sessionStorage.getItem("current_user");
	var drawer = JSON.parse(localStorage[drawerUserName]);
	var drawerCategory = drawer["categories"];

	var match = null;
	var reqID = null;
	var toDisplay = [];
	Object.keys(localStorage).forEach(function(key){
		
		if(!isNaN(key)){
			var data = JSON.parse(localStorage.getItem(key));
			drawerCategory.forEach(category=> {
				if (data["category"]===category){
					reqID = data["id"];
					if(data["state"]==="New")
					toDisplay.push(localStorage.getItem(reqID));
				}
			});
			
		}
	 }); 




    if (toDisplay===[]){
        alert("No request matches your category !");
	}
	else{

	document.getElementById("requests").innerHTML=`
	${toDisplay.map(requestTemplate).join('')}
	`	  
}
}

//Template of the requests waiting to be accepted by the drawers
function requestTemplate(listOfRequest) {
	var request = JSON.parse(listOfRequest);
	console.log(request["id"]);
	return `
	  <div class="request">
	  <p class="requestID">${request["id"]}</p>
	  <p class="requestDreamerUsername">${request["dreamerUserName"]}</p>
	  <p class="requestCategory">${request["category"]}</p>
	  <p class="requestInput">${request["input"]}</p>
	  <p class="requestAccept">
	  <button class="accept" onclick=acceptRequest(${request["id"]})>Accept</button>
	  </p>
	  </div>
	`;
  }

//Accept the request elected by the drawer
function acceptRequest(id){
	var drawerUserName = sessionStorage.getItem("current_user");
	var x = [["drawerUserName",drawerUserName],["state","In progress"]];
	writeInFile(id,x);

displayRequestForDrawer()
}


//Display requests already accepted by the drawer
function displayRequest(){
	//var drawerUserName = sessionStorage.getItem("username");
	var drawerUserName = sessionStorage.getItem("current_user");
	var reqID = null;
	var toDisplay = null;
	var inProgD = document.getElementById("inprogress");

	Object.keys(localStorage).forEach(function(key){
		var data = JSON.parse(localStorage.getItem(key));
		for (var key of Object.keys(data)) {
			if (parsedd["drawerUserName"]==drawerUserName)
				reqID = data["id"];
				toDisplay = localStorage.getItem(reqID);
		}
	 }); 
	
	 if (JSON.parse(toDisplay)["state"]=="In progress"){
		inProgD.appendChild(document.createTextNode(toDisplay));
	 }
}


	
/*
=============================================================================================
Drawer Mode: Drawing upload management
=============================================================================================
*/

//Display the requests accepted by the drawer so that he/she can upload the drawing
  function displayRequestForUpload(){
	
	var drawerUserName = sessionStorage.getItem("current_user");
	var drawer = JSON.parse(localStorage[drawerUserName]);
	var drawerCategory = drawer["categories"];

	var match = null;
	var reqID = null;
	var toDisplay = [];
	Object.keys(localStorage).forEach(function(key){
		
		if(!isNaN(key)){
			var data = JSON.parse(localStorage.getItem(key));
			drawerCategory.forEach(category=> {
				if (data["category"]===category){
					reqID = data["id"];
					if(data["state"]==="In progress"){
						if(data["drawerUserName"]===drawerUserName)
						toDisplay.push(localStorage.getItem(reqID));
					}	
					
				}
			});
			
		}
	 }); 




    if (toDisplay===[]){
        alert("No request matches your category !");
	}
	else{

	document.getElementById("drawing").innerHTML=`
	${toDisplay.map(uploadTemplate).join('')}
	`	  
}
}

//Template of the request to upload the corresponding drawing
function uploadTemplate(listOfRequest) {
	var request = JSON.parse(listOfRequest);
	console.log(request["id"]);
	return `
	  <div class="request">
	  <p class="requestID">${request["id"]}</p>
	  <p class="requestDreamerUsername">${request["dreamerUserName"]}</p>
	  <p class="requestCategory">${request["category"]}</p>
	  <p class="requestInput">${request["input"]}</p>
	  <p class="requestAccept">
	  <input class="input" placeholder="URL of the drawing" id="drawingurl">
	  <button class="accept" onclick=uploadDrawing(${request["id"]})>Upload</button>
	  </p>
	  </div>
	`;
  }

//Display the drawing already uploaded by the drawer
function displayDoneDrawings(){
	
	var drawerUserName = sessionStorage.getItem("current_user");
	var drawer = JSON.parse(localStorage[drawerUserName]);
	var drawerCategory = drawer["categories"];

	var match = null;
	var reqID = null;
	var toDisplay = [];
	Object.keys(localStorage).forEach(function(key){
		
		if(!isNaN(key)){
			var data = JSON.parse(localStorage.getItem(key));
			drawerCategory.forEach(category=> {
				if (data["category"]===category){
					reqID = data["id"];
					if(data["state"]==="Done"){
						if(data["drawerUserName"]===drawerUserName)
						toDisplay.push(localStorage.getItem(reqID));
					}	
					
				}
			});
			
		}
	 }); 




    if (toDisplay===[]){
        alert("No request matches your category !");
	}
	else{

	document.getElementById("done").innerHTML=`
	${toDisplay.map(doneTemplate).join('')}
	`	  
}
}

//Template of the done drawings
function doneTemplate(listOfRequest) {
	var request = JSON.parse(listOfRequest);
	console.log(request["id"]);
	return `
	  <div class="request">
	  <p class="requestID">${request["id"]}</p>
	  <p class="requestDreamerUsername">${request["dreamerUserName"]}</p>
	  <p class="requestCategory">${request["category"]}</p>
	  <p class="requestInput">${request["input"]}</p>
	  <div class="doneDrawing">
  		<img class="photoDreamer" src="${request["url"]}" alt="${request["id"]}">
	  </div>
	  </div>
	`;
  }

// Upload an url of the drawing
function uploadDrawing(id){

	var url =  document.getElementById("drawingurl").value;
	console.log(url);
	var x = [["state","Done"],["url",url]];
	writeInFile(id,x);
	window.location.href = "drawing-sent.html";
}



/*
=============================================================================================
Drawer Mode: Categories management
=============================================================================================
*/

//Add a new category to the list of subscribed categories of the drawer
function addCategory(category){
	var alreadyAdded =false;
	var username = sessionStorage.getItem("current_user");
	console.log(username);
	var user = JSON.parse(localStorage[username]);
	var categories= user["categories"];
	categories.forEach(element => {
		if (element==category) {
			alreadyAdded=true;
		}
	});
	if (!(alreadyAdded)) {
		categories.push(category);
	}
	var x = [['categories',categories]];
	writeInFile(username,x);
	displayMyCategories()
}

//Remove a  category from the list of subscribed categories of the drawer
function removeCategory(category){
	console.log("remove");
	var username = sessionStorage.getItem("current_user");
	var user = JSON.parse(localStorage[username]);
	var categories= user["categories"];
	const index = categories.indexOf(category);
	if (index > -1) {
	categories.splice(index, 1);
	}
	var x = [['categories',categories]];
	writeInFile(username,x);
	displayMyCategories()
}

//Display the Drawer's categories
//Uses of a template to represent each category
function displayMyCategories(){
	var username = sessionStorage.getItem("current_user");
	var user = JSON.parse(localStorage[username]);
	var categories= user["categories"];
	document.getElementById("myCategories").innerHTML=`
	${categories.map(categoryTemplate).join('')}
	`
}

//Get the image of a category
function getCategoryImg(category){
	var img=null;
	if (category=="Pencil") {
		img="https://i.pinimg.com/736x/bb/40/bc/bb40bcb4f86609ab38815191928dd38f--d-pencil-drawings-drawing-with-pencil.jpg";
	}
	if (category=="Manga") {
		console.log("Manga");
		img="https://i-mom.unimedias.fr/2020/09/16/top-10-des-personnages-preferes-de-manga-au-japon.jpg?auto=format%2Ccompress&cs=tinysrgb";
	}
	if (category=="AI") {
		img="https://2s7gjr373w3x22jf92z99mgm5w-wpengine.netdna-ssl.com/wp-content/uploads/2015/03/AI_Brain.jpg";
	}
	if (category=="Abstract"){
		console.log("Abstract");
		img="https://jooinn.com/images/abstract-segment-1.jpg";
	}
	return img;
}

//Template of the category
function categoryTemplate(category) {
	return `
	  <div class="category">
	  <img class="category-photo-remove" src=${getCategoryImg(category)} onclick=removeCategory("${category}")>
	  <h3 class="category-name">${category}
	  </div>
	`;
  }



/*
=============================================================================================
General functions: Session management
=============================================================================================
*/


//Logout the current user
  function logOut(){
	sessionStorage.setItem("current_user","");
  }

//Get the username of the current user
function getUsername(){
	var username = sessionStorage.getItem("current_user");
	var text = document.getElementById("username"); 
	text.appendChild(document.createTextNode(username)); 
}

//Check if an user is currently logged
function isUserLogged(){
	var username = sessionStorage.getItem("current_user");
	if(username===""){
		window.location.href='Login.html';
	}
}


