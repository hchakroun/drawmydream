function writeInFile(data,x){

	var user = JSON.parse(localStorage[data]);
	x.forEach(element => {
		user[element[0]]=element[1];
		localStorage[data] = JSON.stringify(user);
	});

}

function sub(){
    const username=document.getElementById("uname").value;
    const firstname=document.getElementById("firstname").value;
    const lastname=document.getElementById("lastname").value;
    const birthdate=document.getElementById("birthdate").value;
    const email=document.getElementById("email").value;
    const password=document.getElementById("pwd").value;
    const match=JSON.parse(localStorage.getItem(username));
    var today = new Date().toJSON().slice(0,10).replace(/-/g,'-');
    if (!isNaN(username)){ alert("you can't use a numeric username") } 
    else if(match!==null){ alert("This username has already been used. Choose another.") } 
    else if ( today < birthdate){alert("Invalid Birthday")}
    else {

        var obj = `{
         "firstname" : "firstname",
         "lastname": "lastname",
         "username": "username",
         "email": "email",
         "password": "password",
         "birthday": "birthdate",
          "categories": []
        }`
    localStorage.setItem(username,obj);
    const L=[['firstname',firstname],['lastname',lastname],['username',username],['email',email],['password',password],['birthday',birthdate]]
    writeInFile(username,L);
    if (confirm('Signed Up'))
    {
        window.location.replace('Login.html')}}
}